require 'rails_helper'

RSpec.describe "Weather", type: :request do
  describe "GET /new" do
    it "returns http success" do
      get "/weather/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST /create" do
    let(:forecast_service) { instance_double(Services::Weather::CachedWeatherForecastService) }
    let(:forecast) do
      { :current_temp => 67.2, 
        :days_forecast => [
          { :date => "2024-05-30",
          :min_temp => 51.9, 
          :max_temp => 70.4, 
          :conditions => "Partially cloudy" }
          ]
      }
    end

    before do
      allow(Services::Weather::CachedWeatherForecastService).to receive(:new).and_return(forecast_service)
      allow(forecast_service).to receive(:forecast).and_return(Result.ok(forecast))
    end

    it "returns http success" do
      post "/weather/create", as: :turbo_stream, params: { location: "New York" }

      expect(forecast_service).to have_received(:forecast).with(no_args).once
      expect(Services::Weather::CachedWeatherForecastService).to have_received(:new).with(location: 'New York').once
      expect(response).to have_http_status(:success)
    end
  end
end
