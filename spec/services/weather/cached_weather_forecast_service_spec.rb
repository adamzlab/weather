require 'rails_helper'

RSpec.describe Services::Weather::CachedWeatherForecastService do
  describe '#forecast' do
    subject(:forecast) { cached_weather_forecast_service.forecast }

    let(:location) { 'New York' }
    let(:provider) { instance_double(Services::Weather::VisualCrossingAdapter) }
    let(:expires_in) { 30.minutes }
    let(:forecast_service) { instance_double(Services::Weather::WeatherForecastService) }
    let(:cached_weather_forecast_service) { described_class.new(location: location, provider: provider, expires_in: expires_in) }
    let(:forecast_data_result) do
      Result.ok(
          { 
            current_temp: 67.2, 
             days_forecast: [
              { date: '2024-05-30',
                min_temp: 51.9,
                max_temp: 70.4,
                 conditions: 'Partially cloudy' 
              }]
          })
    end

    before do
      allow(Services::Weather::WeatherForecastService).to receive(:new).and_return(forecast_service)
      allow(forecast_service).to receive(:forecast).and_return(forecast_data_result)
    end

    context 'when location is blank' do
      let(:location) { '' }

      it 'returns an error result' do
        expect(forecast.payload).to eq('Invalid location provided. Please try again.')
      end
    end

    context 'when location is not blank' do
      before do
        allow(Rails.cache).to receive(:read).with(location).and_return(nil)
        allow(Rails.cache).to receive(:write).with(location, forecast_data_result, expires_in: expires_in)
      end

      it 'returns the forecast from the forecast service' do
        expect(forecast.payload[:cache_expires_in_mins]).to eq(nil)
        expect(forecast.payload).to eq(forecast_data_result.payload)
      end

      it 'caches the forecast' do
        forecast

        expect(Rails.cache).to have_received(:read).with(location).once
        expect(forecast_service).to have_received(:forecast).once
        expect(Rails.cache).to have_received(:write).with(location, forecast_data_result, expires_in: expires_in).once
      end
    end
  end
end
