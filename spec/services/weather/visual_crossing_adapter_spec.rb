require "rails_helper"

RSpec.describe Services::Weather::VisualCrossingAdapter do
  describe "#forecast" do
    subject(:forecast) { Services::Weather::VisualCrossingAdapter.new(location).forecast }

    let(:location) { "New York" }
    let(:http_mock) { instance_double(Net::HTTP) }
    let(:response_mock) { instance_double(Net::HTTPResponse) }
    let(:visual_crossing_response) do
      '{
        "queryCost": 1,
        "latitude": 40.089811,
        "longitude": -83.13983,
        "resolvedAddress": "43016, USA",
        "address": "43016",
        "timezone": "America/New_York",
        "tzoffset": -4,
        "days": [
          {
            "datetime": "2024-05-30",
            "datetimeEpoch": 1717041600,
            "tempmax": 70.1,
            "tempmin": 51.9,
            "temp": 61.2,
            "feelslikemax": 70.1,
            "feelslikemin": 51.9,
            "feelslike": 61.2,
            "dew": 44.4,
            "humidity": 57.9,
            "precip": 0,
            "precipprob": 0,
            "precipcover": 0,
            "preciptype": null,
            "snow": 0,
            "snowdepth": 0,
            "windgust": 13.9,
            "windspeed": 9.2,
            "winddir": 8.9,
            "pressure": 1021.8,
            "cloudcover": 27.1,
            "visibility": 10,
            "solarradiation": 358.9,
            "solarenergy": 31,
            "uvindex": 10,
            "severerisk": 10,
            "sunrise": "06:06:16",
            "sunriseEpoch": 1717063576,
            "sunset": "20:54:40",
            "sunsetEpoch": 1717116880,
            "moonphase": 0.75,
            "conditions": "Partially cloudy",
            "description": "Partly cloudy throughout the day.",
            "icon": "partly-cloudy-day",
            "stations": [
              "KDLZ",
              "KOSU",
              "F4387",
              "KMRT"
            ],
            "source": "comb"
          },
          {
            "datetime": "2024-05-31",
            "datetimeEpoch": 1717128000,
            "tempmax": 74.9,
            "tempmin": 47,
            "temp": 61.9,
            "feelslikemax": 74.9,
            "feelslikemin": 47,
            "feelslike": 61.9,
            "dew": 42.7,
            "humidity": 52.7,
            "precip": 0,
            "precipprob": 0,
            "precipcover": 0,
            "preciptype": null,
            "snow": 0,
            "snowdepth": 0,
            "windgust": 6.9,
            "windspeed": 3.4,
            "winddir": 102.6,
            "pressure": 1023,
            "cloudcover": 8,
            "visibility": 10.1,
            "solarradiation": 371,
            "solarenergy": 31.9,
            "uvindex": 9,
            "severerisk": 10,
            "sunrise": "06:05:49",
            "sunriseEpoch": 1717149949,
            "sunset": "20:55:24",
            "sunsetEpoch": 1717203324,
            "moonphase": 0.79,
            "conditions": "Clear",
            "description": "Clear conditions throughout the day.",
            "icon": "clear-day",
            "stations": null,
            "source": "fcst"
          }
        ],
        "stations": {
          "OH170": {
            "distance": 1136,
            "latitude": 40.099,
            "longitude": -83.135,
            "useCount": 0,
            "id": "OH170",
            "name": "I270 @ US33 (COLS)",
            "quality": 0,
            "contribution": 0
          },
          "KDLZ": {
            "distance": 22349,
            "latitude": 40.29,
            "longitude": -83.12,
            "useCount": 0,
            "id": "KDLZ",
            "name": "KDLZ",
            "quality": 98,
            "contribution": 0
          },
          "KOSU": {
            "distance": 6047,
            "latitude": 40.08,
            "longitude": -83.07,
            "useCount": 0,
            "id": "KOSU",
            "name": "KOSU",
            "quality": 100,
            "contribution": 0
          },
          "F4387": {
            "distance": 6913,
            "latitude": 40.047,
            "longitude": -83.081,
            "useCount": 0,
            "id": "F4387",
            "name": "FW4387 Columbus OH US",
            "quality": 0,
            "contribution": 0
          },
          "AV747": {
            "distance": 7673,
            "latitude": 40.021,
            "longitude": -83.15,
            "useCount": 0,
            "id": "AV747",
            "name": "N8XE Hilliard OH US",
            "quality": 0,
            "contribution": 0
          },
          "KMRT": {
            "distance": 23017,
            "latitude": 40.22,
            "longitude": -83.35,
            "useCount": 0,
            "id": "KMRT",
            "name": "KMRT",
            "quality": 99,
            "contribution": 0
          }
        },
        "currentConditions": {
          "datetime": "11:05:00",
          "datetimeEpoch": 1717081500,
          "temp": 62,
          "feelslike": 62,
          "humidity": 47.5,
          "dew": 41.8,
          "precip": 0,
          "precipprob": 0,
          "snow": 0,
          "snowdepth": 0,
          "preciptype": null,
          "windgust": 4.1,
          "windspeed": 1.1,
          "winddir": 55,
          "pressure": 1023,
          "visibility": 0.3,
          "cloudcover": 88,
          "solarradiation": 786,
          "solarenergy": 2.8,
          "uvindex": 8,
          "conditions": "Partially cloudy",
          "icon": "fog",
          "stations": [
            "KOSU",
            "OH170",
            "AV747"
          ],
          "source": "obs",
          "sunrise": "06:06:16",
          "sunriseEpoch": 1717063576,
          "sunset": "20:54:40",
          "sunsetEpoch": 1717116880,
          "moonphase": 0.75
        }
      }'
    end
    let(:api_response_status) { 200 } 

    before do
      url = Services::Weather::VisualCrossingAdapter::API_BASE_URL
      url += "New+York?contentType=json&include=current&key=#{Services::Weather::VisualCrossingAdapter::API_KEY}&unitGroup=us"
      stub_request(:get, url).to_return(status: api_response_status, body: visual_crossing_response.as_json.to_s, headers: {})
    end

    context "with a successful API call" do
      it "returns a response with weather forecast" do
        expect(forecast.payload[:current_temp]).to eq(62)
        expect(forecast.payload[:days_forecast]).to match_array(
          [
            {
              conditions: "Partially cloudy", 
              date: "2024-05-30",
              max_temp: 70.1,
              min_temp: 51.9
            },
            {
              conditions: "Clear",
              date: "2024-05-31",
              max_temp: 74.9,
              min_temp: 47
            }
          ])
      end
    end

    context "with a failed API call" do
      let(:visual_crossing_response) { 'Bad API Request:Invalid location parameter value.'}
      let(:api_response_status) { 500 } 

      it "returns an error result" do
        expect(forecast.payload).to eq(Services::Weather::VisualCrossingAdapter::INVALID_LOCATION_ERROR)
      end
    end
  end
end
