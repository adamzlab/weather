require "rails_helper"

RSpec.describe "weather/new.html.erb", type: :view do
  it "renders the form" do
    render

    expect(rendered).to match("Enter the address, get the forecast!")
  end
end
