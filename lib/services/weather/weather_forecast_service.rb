# Retrieves the forecast in the form of a hash
# { :current_temp => 67.2, 
# :days_forecast => [
#   { :date => "2024-05-30",
#    :min_temp => 51.9, 
#    :max_temp => 70.4, 
#    :conditions => "Partially cloudy" }
#   ]}
# Each provider class is responsible for parsing the data from the API
module Services::Weather
  class WeatherForecastService
    delegate :forecast, to: :@provider

    def initialize(location:, provider: VisualCrossingAdapter)
      @location = location
      @provider = provider.new(location)
    end
  end
end
