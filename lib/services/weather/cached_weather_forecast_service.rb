module Services::Weather
  class CachedWeatherForecastService
    def initialize(location:, provider: VisualCrossingAdapter, expires_in: 30.minutes)
      @location = location
      @expires_in = expires_in
      @service = WeatherForecastService.new(location: location, provider: provider)
    end

    def forecast
      return Result.error("Invalid location provided. Please try again.") if @location.blank?

      result = Rails.cache.read(@location)

      if result&.ok?
        result.payload[:cache_expires_in_mins] = @expires_in / 60
        return result
      end

      result = @service.forecast
      result.payload[:cache_expires_in_mins] = nil if result.ok?
      Rails.cache.write(@location, result, expires_in: @expires_in)
      result
    end
  end
end
