require "uri"
require "net/http"
require "openssl"

# This class is responsible for connecting to the Visual Crossing Weather API
# and parsing the data into a format that the application can use.
# It returns a Result object with the forecast data if the request is successful,
# or an error message if the request fails.
module Services::Weather
  class VisualCrossingAdapter
    API_BASE_URL = "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/"
    # This would be a secret
    API_KEY = "XSPGR3Q6W3V5EU4PGTAU9UBW8"
    LOCALE = "us"
    INVALID_LOCATION_ERROR = "Invalid location provided. Please try again."

    # location can be a zip code, a city name, or an address
    def initialize(location)
      @location = location
    end

    def forecast
      response = call_api
      if response.code != "200"
        return Result.error(INVALID_LOCATION_ERROR)
      end

      weather = JSON.parse(response.read_body)
      current_conditions = weather["currentConditions"]
      days_forecast = weather["days"].map { |d| parse_day_forecast(d) }

      Result.ok(
        {
          current_temp: current_conditions["temp"],
          days_forecast: days_forecast,
        }
      )
    rescue JSON::ParserError
      Result.error(INVALID_LOCATION_ERROR)
    rescue StandardError
      Result.error('An error occurred while fetching the weather data. Please try again.')
    end

    private

    def parse_day_forecast(day)
      {
        date: day["datetime"],
        min_temp: day["tempmin"],
        max_temp: day["tempmax"],
        conditions: day["conditions"],
      }
    end

    def call_api
      url = URI(api_url)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(url)
      http.request(request)
    end

    def api_url
      encoded_location = CGI.escape(@location)
      "#{API_BASE_URL}#{encoded_location}?unitGroup=#{LOCALE}&include=current&key=#{API_KEY}&contentType=json"
    end
  end
end
