# frozen_string_literal: true

# Inspired from https://doc.rust-lang.org/std/result/
class Result
  class RestultError < StandardError; end

  KINDS = [:ok, :error].freeze

  class << self
    def error(payload = nil)
      new(:error, payload: payload)
    end

    def ok(value = nil)
      new(:ok, payload: value)
    end
  end

  def initialize(kind, payload: nil)
    unless KINDS.include?(kind)
      raise ArgumentError.new("Unknown result kind: #{@kind}")
    end

    @kind = kind
    @payload = payload
  end

  def ok?
    @kind == :ok
  end

  def error?(reason = nil)
    @kind == :error
  end

  def payload
    @payload
  end
end