class WeatherController < ApplicationController
  def new; end

  def create
    @weather_result = Services::Weather::CachedWeatherForecastService.new(location: params[:location]).forecast

    respond_to do |format|
      format.turbo_stream
    end
  end
end
