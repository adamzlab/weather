# README

Weather web app connects to a remote weather service, retrieves weather info and shows it in the UI.


## Ruby version

3.2.0

## How to run

Without docker:  `bundle exec rails s`

With docker:

`docker build -t weather_app .`


`docker run --rm -it -p 3000:3000 --env RAILS_MASTER_KEY={your_key_from_master.key} weather_app`

Make sure to replace RAILS_MASTER_KEY env variable.

Access at `http://127.0.0.1:3000/`

## Architecture

This project uses Rails Turbo framework to push update to the frontend. When a user submits a location, it is sent to the backend, where we url-encode it and send it the weather service provider.

When the weather service provider gives us a response, we cache it for the next calls, and send just the necessary html to the frontend.

UI can accept an address, a city, or a zip code.

If the data comes from the cache, then the UI will show a message "Data can be up to 30 minutes old"

### Backend Architecture

When a user submits a location, it is sent to `WeatherController#create`. The controller method then calls `CachedWeatherForecastService` to get the forecast for the submitted location.

`CachedWeatherForecastService` is a simple decorator around `WeatherForecastService`, which adds a caching capability. `CachedWeatherForecastService` calls `WeatherForecastService` if the cache is empty, otherwise it retrieves data from he cache.

`WeatherForecastService` delegates the forecast to specific weather providers. For this project, there is currently only a single weather service provider - Visual Crossing - and it uses `VisualCrossingAdapter` class to call the service.


## How to run the test suite

`bundle exec rspec`

### Tests coverage

- Request specs in spec/requests/weather_spec.rb
- Unit tests for the services
- Basic view tests

Web request mocking is done using a webmock gem


# App Screenshots
<details>
  <summary>Initial Page</summary>

  ![alt initial page](screenshots/initial_page.png "Initial Page")

</details>

<details>
  <summary>Fresh, not Cached Search Results</summary>

  ![alt initial page](screenshots/fresh_page.png "Initial Page")

</details>

<details>
  <summary>Cached Search Results</summary>

  ![alt initial page](screenshots/cached_page.png "Initial Page")

</details>


